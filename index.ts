'use strict';

import {Server, Request, ResponseToolkit} from "@hapi/hapi";
import * as path from "path";

const { Sequelize, Model, DataTypes } = require('sequelize');
//user: postgres
//password: 123321
//database: crypton_bd
//postgresql port: 5432
const sequelize = new Sequelize('postgres://postgres:123321@localhost:5432/crypton_bd')
sequelize.options.logging = false;

//table init
class User extends Model {}
User.init({
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true
    },
    firstName: {
        type: DataTypes.STRING,
        allowNull: false
        },
    lastName: DataTypes.STRING
}, { sequelize, modelName: 'users' });

//filling database with users
async function fill_db(){
    //if table 'users' doesn't exist then sync() will add it
    await sequelize.sync();
    //everything that happens to be inside 'users' table will be deleted 
    await User.destroy({
       truncate: true
    });
    await Promise.all([
        User.create({ firstName: "Jane", lastName: "Mary" }),
        User.create({ firstName: "Oliver", lastName: "Destroyer" }),
        User.create({ firstName: "Nick", lastName: "olas Cage" }),
        User.create({ firstName: "Ivan", lastName: "Chuvaev" }),
        User.create({ firstName: "Simple", lastName: "Dimple" }),
        User.create({ firstName: "Noize", lastName: "MC" }),
        User.create({ firstName: "Rimuru", lastName: "Tempesto" }),
        User.create({ firstName: "Tesla", lastName: "Electronics" }),
        User.create({ firstName: "James", lastName: "Born" }),
        User.create({ firstName: "Misaka", lastName: "Mikoto" })
    ]);
};

async function test_bd(){
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
        return true;
      } catch (error) {
        console.error('Unable to connect to the database:', error);
        return false
      }
}

const init = async () => {

    const server = new Server({
        port: 3000,
        host: 'localhost',
        routes: {
            files: {
                relativeTo: path.join(__dirname, 'public')
            }
        }
    });

    server.route({
        method: 'GET',
        path: '/',
        handler: (request: Request, reply: ResponseToolkit) => {

            return reply.redirect('/list_all_users');
        }
    });

    server.route({
        method: 'GET',
        path: '/user',
        handler: async (request: Request, reply: ResponseToolkit) => {
            const user = await User.findOne({
                where: {id:request.query.id}
            })
            
            if(user)
                return "hello " + user.firstName + ' ' + user.lastName;

            return "user not found";
        }
    });

    server.route({
        method: 'GET',
        path: '/list_all_users',
        handler: async (request: Request, reply: ResponseToolkit) => {
            const users = await User.findAll();
            
            if(users){
                let str = "";
                for (let user of users){
                    str += "<a href='/user?id=" + user.id + "'>" + user.firstName + ' ' + user.lastName + '</a><br>';
                }
                return str;
            }

            return "database is empty";
        }
    });

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

(async ()=>{
    if(await test_bd()){
        await fill_db();
        init();
    }
})();